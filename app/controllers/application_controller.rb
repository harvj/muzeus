class ApplicationController < ActionController::Base
  require 'will_paginate/array'

  attr_reader :query
  helper_method :query

  protect_from_forgery with: :exception
  before_action :store_current_path

  def lastfm
    response = Connectors::LastFm.new.next_play
    @response = response.results

    if @response
      match_next      if params[:log_next] == 'true'
      match_specific  if params[:specific_id].present?

      @lookup = MuzeusMatch.transaction do
        MuzeusMatch::Lookup.(play_params)
      end

      log_recordings
    else
      flash[:error] = response.errors
    end

    respond_to do |format|
      format.html { redirect_to params[:redirect] || plays_path }
      format.json { render json: @response.to_json }
    end
  end

  def musicbrainz
    response = MusicBrainz::QueryRecording.(query: 'becs', artist: 'fennesz')

    response = MusicBrainz::GetRelease.(response.results[:release])

    respond_to do |format|
      format.json { render json: response.to_json }
    end
  end

  def new_session_path(scope)
    new_user_session_path
  end

  private

  def refresh_db_views
    ApplicationRecord.connection.execute("REFRESH MATERIALIZED VIEW played_recordings")
    ApplicationRecord.connection.execute("REFRESH MATERIALIZED VIEW played_releases")
    ApplicationRecord.connection.execute("REFRESH MATERIALIZED VIEW played_artists")
  end

  def log_recordings
    played_at = Time.at(@response[:utc].to_i)
    @lookup.result.each_with_index do |recording, index|
      played_at += (recording.length/1000) if index > 0
      recording.plays.create!(played_at: played_at.to_datetime, release_id: recording.releases.first.id)
      refresh_db_views
    end
  end

  def match_next
    new_muzeus_match(Play.last.recording_id+1)
  end

  def match_specific
    new_muzeus_match(params[:specific_id].to_i)
  end

  def new_muzeus_match(id)
    name = MuzeusMatch.encode(play_params)
    muzeus_match = MuzeusMatch.create(play_params.merge(name: name))
    recording = Recording.find(id)
    muzeus_match.recordings << recording
  end

  def play_params
    @response.slice(:recording_string, :release_string, :artist_string)
  end

  def store_current_path
    @current_path = request.url
  end

  def per_page
    @per_page || params[:per_page] || 40
  end

  def page_title
    @page_title || 'Muzeus'
  end
  helper_method :page_title

  def parse_params
    @query = {} unless @query.present?
    ApplicationRecord::ACCEPTED_QUERY_KEYS.each do |key|
      if params[key].present?
        @query[key] = params[key]
      end
    end
    @filtered_artists = Artist.where(id: params[:artist_id].to_s.split(',')).select(:id, :name) if params[:artist_id].present?
  end

  def results_are_filtered?(*scopes)
    if scopes.any?
      results = {
        by_date: (ApplicationRecord::PLAYED_AT_QUERY_KEYS + [:date]).without(:play_year).any? { |key| query[key].present? },
        by_t: query[:t].present?
      }
      scopes.any? { |scope| results[scope] }
    else
      ApplicationRecord::COMBINABLE_QUERY_KEYS.any? { |key| query[key].present? }
    end
  end
  helper_method :results_are_filtered?

  def with_existing_query(key=nil, value=nil, options={})
    return {} if query.nil? && key.nil?
    return query.slice(*ApplicationRecord::ACCEPTED_QUERY_KEYS) if key.nil?
    if key.is_a?(Array)
      values = value.map(&:to_s)
      default = Hash[key.zip(values)]
    else
      value = value.to_s
      default = { key => value }
    end
    return default if query.nil?
    result = query.slice(*ApplicationRecord::ACCEPTED_QUERY_KEYS).except(:page)
    if ApplicationRecord::COMBINABLE_QUERY_KEYS.include?(key)
      current_values = query[key].present? ? query[key].split(',') : []
      if current_values.include?(value)
        return result.except(key) if current_values.length == 1
        result.merge(key => (current_values - [value]).join(','))
      else
        result.merge(key => (current_values + [value]).join(','))
      end
    else
      if key.is_a?(Array)
        match_key = options[:remove_match_key]
        return result.except(match_key) if query[match_key] == default[match_key]
      else
        return result.except(key) if query[key].present? && query[key].to_s == value
      end
      result.merge(default).merge(options.slice(*ApplicationRecord::ACCEPTED_QUERY_KEYS))
    end
  end
  helper_method :with_existing_query

  def toggle_order(key)
    q = query || {}
    toggles = {
      'artist-name' => q[:order] == 'artist-name-a' ? 'artist-name-z' : 'artist-name-a',
      'category' => q[:order] == 'category-a' ? 'category-z' : 'category-a',
      'recently-added' => q[:order] == 'recently-added' ? 'not-recently-added' : 'recently-added',
      'recently-discovered' => q[:order] == 'recently-discovered' ? 'not-recently-discovered' : 'recently-discovered',
      'recently-played' => q[:order] == 'recently-played' ? 'not-recently-played' : 'recently-played',
      'length' => q[:order] == 'longest' ? 'shortest' : 'longest',
      'most-played' => q[:order] == 'most-played' ? 'least-played' : 'most-played',
      'name' => q[:order] == 'name-a' ? 'name-z' : 'name-a',
      'overall-rank' => q[:order] == 'overall-rank-1' ? 'overall-rank-9' : 'overall-rank-1',
      'sub-most-played' => q[:order] == 'most-played-in-group' ? 'least-played-in-group' : 'most-played-in-group',
      'sub-recently-discovered' => q[:order] == 'recently-discovered-in-group' ? 'not-recently-discovered-in-group' : 'recently-discovered-in-group',
      'sub-recently-played' => q[:order] == 'recently-played-in-group' ? 'not-recently-played-in-group' : 'recently-played-in-group',
      'year' => q[:order] == 'recent-release' ? 'not-recent-release' : 'recent-release'
    }
    toggles[key]
  end
  helper_method :toggle_order
end
