class PlaysController < ApplicationController
  before_action :parse_params, only: %i(index)

  def index
    @page_title = "Plays"
    @plays = Play.ranked(query).paginate(per_page: per_page, page: params[:page])
  end

  def create
    prev = Play.last
    recording = if params[:recording_id].present?
      Recording.find(params[:recording_id])
    else
      Recording.find(prev.recording_id + 1)
    end

    at = if params[:use_time_params].present? && params[:recording_id].present?
      at = Time.local(params[:year],params[:month],params[:day],params[:hour],params[:minute],0)
    else
      at = Time.at(prev.played_at + recording.length/1000)
    end

    Play.create(
      played_at: at,
      recording_id: recording.id,
      release_id: recording.releases.first.id
    )
    refresh_db_views
    redirect_to params[:form_redirect]
  end

  private

  def parse_params
    super
    query[:date] = Date.today.strftime('%Y%m%d') if query[:date] == 'today'
  end

  def per_page
    query[:date] ? 1000 : 30
  end
end
