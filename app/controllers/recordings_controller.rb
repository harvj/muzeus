class RecordingsController < ApplicationController
  before_action :parse_params, only: %i(index show)

  def index
    @page_title = "Search > #{query[:name]}" if query[:name].present?
    @recordings = Recording.ranked(query).paginate(page: params[:page], per_page: 40)
  end

  def show
    @recording = Recording.ranked(id: params[:id])&.first
    @plays     = Play.ranked(recording_id: @recording.id) if @recording.present?
  end

  def update
    @recording = Recording.find(params[:id])
    Recording::Update.(@recording, recording_params)
    redirect_to params[:form_redirect] || recording_path(@recording)
  end

  private

  def recording_params
    params.require(:recording).permit(:category)
  end
end
