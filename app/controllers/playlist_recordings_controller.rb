class PlaylistRecordingsController < ApplicationController
  def create
    PlaylistRecording.create!(playlist_recording_params)
    redirect_to playlist_path(playlist_recording_params[:playlist_id])
  end

  private

  def playlist_recording_params
    params.require(:playlist_recording).permit(:playlist_id, :recording_id, :position)
  end
end
