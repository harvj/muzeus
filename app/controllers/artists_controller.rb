class ArtistsController < ApplicationController
  require 'will_paginate/array'

  before_action :parse_params, only: %i(index show)

  def roster
    @artists = Artist.ranked.joins(recordings: :plays).order(:name).where("plays.played_at > '#{2.days.ago}'").limit(5)
    render :index
  end

  def index
    if query[:rank].present?
      artist = Artist.ranked_with_subrank(query).first
      redirect_to artist_path(artist) and return if artist.present?
    end

    @page_title = "Artists"
    @page_title += " > #{query_title}" if query_title.present?
    @artists = Artist.ranked(query)

    if query[:t].present?
      t = query[:t].to_i
      if !results_are_filtered?(:by_date)
        @superlist = Artist.ranked(limit: 1, t: t - 1, order: 'least-played') if t > 1
        @sublist   = Artist.ranked(limit: 1, t: t + 1, order: 'most-played')
      end
      @artists  = [@superlist, @artists, @sublist].compact.concat.flatten
    else
      @artists = @artists.paginate(page: params[:page], per_page: per_page)
    end
  end

  def show
    artist = Artist.find(params[:id])
    @artist = Artist.by_id(artist.id, category: artist.category).first
    @page_title = "Artist > #{@artist.name}"
    @releases = Release.ranked(artist_id: @artist.id, order: 'most-played', limit: 10)
    @recordings = Recording.ranked(artist_id: @artist.id, order: 'most-played', limit: 5)
    @plays_by_year = Artists::PlaysByYear.(artist_id: @artist.id)
  end

  def update
    @artist = Artist.find(params[:id])
    Artist::Update.(@artist, artist_params.merge(update_options))
    redirect_to params[:form_redirect] || artist_path(@artist)
  end

  private

  def query_title
    if query[:category].present?
      query[:category]
    elsif query[:order].present?
      query[:order]
    end
  end

  def artist_params
    params.require(:artist).permit(:category, :status)
  end

  def update_options
    params.to_unsafe_h.slice(:cascade_categories)
  end
end
