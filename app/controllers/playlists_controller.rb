class PlaylistsController < ApplicationController
  def index
    @playlists = Playlist.for_index.order('date DESC')
  end

  def show
    @playlist = Playlist.find(params[:id])
    @page_title = "Playlist > #{@playlist.name}"
    @recordings = Recording.for_playlist(@playlist)
  end
end
