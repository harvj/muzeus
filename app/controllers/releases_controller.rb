class ReleasesController < ApplicationController
  require 'will_paginate/array'

  def index
    if params[:rank].present?
      release = Releases::Find.(rank: params[:rank])&.first
      redirect_to release_path(release) and return if release.present?
    end

    parse_params
    set_index_title
    @releases  = Release.ranked(query)

    if query[:t].present?
      t = query[:t].to_i
      @superlist = Release.ranked(limit: 1, t: t - 1, order: 'least-played') if t > 1
      @sublist   = Release.ranked(limit: 1, t: t + 1, order: 'most-played')
      @releases  = [@superlist, @releases, @sublist].compact.concat.flatten
    else
      @releases = @releases.paginate(page: params[:page], per_page: per_page)
    end
  end

  def update
    @release = Release.find(params[:id])
    Release::Update.(@release, release_params.merge(update_options))
    redirect_to params[:form_redirect] || release_path(@release)
  end

  def show
    @release    = Release.ranked(id: params[:id])&.first
    @recordings = Recording.for_release(params[:id])
    @page_title = "Releases > #{@release.name}"
    @plays_by_year = Releases::PlaysByYear.(release_id: params[:id])
  end

  def import
    @release = MusicBrainz::ImportRelease.(params[:mbid]).result
    redirect_to release_path(@release)
  end

  private

  def parse_params
    if params[:year].present?
      @plays_by_release_year = Releases::PlaysByReleaseYear.()
    end
    super
  end

  def set_index_title
    title = ['Releases', params[:t], params[:year], params[:category], params[:status]].compact.join(' > ')
    title += " > p.#{params[:page]}" if params[:page]
    @page_title = title
  end

  def release_params
    params.require(:release).permit(:category, :status)
  end

  def update_options
    params.to_unsafe_h.slice(:cascade_categories)
  end
end
