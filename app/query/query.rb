class Query
  def self.call(options={})
    new(options).send(:call)
  end

  def self.categories(query)
    query.split(',').map { |c| ApplicationRecord::CATEGORIES[c.to_sym] }
  end

  attr_reader :options, :filters

  def initialize(options={})
    @options = options
    @filters = Hash.new { |hash, key| hash[key] = [] }
  end

  private

  def call
    set_filters
    self.class.parent_name.singularize.constantize.find_by_sql(query)
  end

  def query
    raise NotImplementedError, 'Subclass responsibility'
  end

  def set_filters
  end

  def filter(key, query_string, override: false)
    if override
      @filters[key] = [query_string]
    else
      @filters[key] << query_string
    end
  end

  def where(key)
    return if filters[key].empty?
    " WHERE #{filters[key].join(' AND ')} "
  end
end
