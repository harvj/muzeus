class Artists::PlaysByYear < Query
  private

  def query
    <<~SQL
      SELECT
        EXTRACT(year FROM played_at) AS year,
        count(*)                     AS play_count
      FROM plays
      INNER JOIN recordings ON recordings.id = plays.recording_id
      INNER JOIN recording_artists ra ON recordings.id = ra.recording_id
      INNER JOIN artists ON artists.id = ra.artist_id
      WHERE artists.id = #{options[:artist_id]}
      GROUP BY year
      ORDER BY year
    SQL
  end
end
