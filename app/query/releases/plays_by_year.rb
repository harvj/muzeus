class Releases::PlaysByYear < Query
  private

  def query
    <<~SQL
      SELECT
        EXTRACT(year FROM played_at) AS year,
        count(*)                     AS play_count
      FROM plays
      INNER JOIN recordings ON recordings.id = plays.recording_id
      INNER JOIN release_recordings rr ON recordings.id = rr.recording_id
      WHERE rr.release_id = #{options[:release_id]}
      GROUP BY year
      ORDER BY year
    SQL
  end
end
