class Releases::PlaysByReleaseYear < Query
  private

  def query
    <<~SQL
      SELECT rel.year,
        SUM(r.length) / 1000 / 60 / 60 AS play_count
      FROM plays p
      INNER JOIN recordings r ON r.id = p.recording_id
      INNER JOIN releases rel ON rel.id = p.release_id
      GROUP BY rel.year
      ORDER BY rel.year DESC
    SQL
  end
end
