class MatchRecording < ApplicationRecord
  belongs_to :muzeus_match
  belongs_to :recording

  validates_presence_of :muzeus_match, :recording
  validates_uniqueness_of :recording_id, scope: :muzeus_match_id
end
