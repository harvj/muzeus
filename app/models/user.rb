class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable
  # :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable
  devise :omniauthable, omniauth_providers: %i[spotify]
  has_many :plays

  # validates_presence_of :last_fm_id

  def self.from_omniauth(auth)
    user = find_or_initialize_by(provider: auth.provider, uid: auth.uid)
    user.email = auth.info.email
    user.name = auth.info.name
    user.spotify_profile_url = auth.info.urls.spotify
    user.spotify_access_token = auth.credentials.token
    user.spotify_refresh_token = auth.credentials.refresh_token
    user.spotify_token_expires_at = Time.at auth.credentials.expires_at

    user.save!
    user
  end
end
