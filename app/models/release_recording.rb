class ReleaseRecording < ApplicationRecord
  belongs_to :release
  belongs_to :recording

  validates :release_id, uniqueness: { scope: :recording_id }
end
