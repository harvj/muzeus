class Artist < ApplicationRecord
  enum category: CATEGORIES
  enum status: STATUSES

  has_many :release_artists, dependent: :destroy
  has_many :releases, through: :release_artists

  has_many :recording_artists, dependent: :destroy
  has_many :recordings, through: :recording_artists

  validates_presence_of :name, :mbid
  validates_uniqueness_of :mbid

  def self.rank_column
    'time_played'
  end

  def self.icon
    "<i class='fa-solid fa-user'></i>"
  end

  def self.by_id(id, params={})
    find_by_sql(
      <<~SQL.strip_heredoc
        SELECT subranked.*
        FROM (#{_ranked(params)}) as subranked
        WHERE subranked.id = #{id}
      SQL
    )
  end

  def self.ranked(params={})
    find_by_sql(_ranked(params))
  end

  def self._ranked(params)
    <<-SQL.strip_heredoc
      SELECT artists.*,
        played_artists.time_played,
        played_artists.num_plays,
        played_artists.last_played,
        played_artists.first_played,
        played_artists.overall_rank,
        played_artists.t_value
        #{get_subrank(params)}
        #{played_at_subselects('artists', params)}
      FROM artists
      JOIN played_artists ON played_artists.artist_id = artists.id
      #{join_subplays(params)}
      #{set_filters(params)}
      #{set_order('artists', params)}
      #{set_limit(params)}
    SQL
  end

  def self.set_filters(params)
    sql = []
    sql << Artist.sanitize_sql(["artists.id IN (?)", params[:id]]) if params[:id].present?
    sql << Artist.sanitize_sql(["overall_rank = ?", params[:rank]]) if params[:rank].present?
    sql << filter_enum(Artist, 'category', params[:category]) if params[:category].present?
    sql << filter_enum(Artist, 'status', params[:status]) if params[:status].present?
    sql << Artist.sanitize_sql(["LOWER(name) LIKE LOWER(?)", "%#{params[:name]}%"]) if params[:name].present?
    sql << "sub_num_plays > 0" if sub_select_plays_by_played_at_time?(params)
    sql << Artist.sanitize_sql(["t_value = ?", params[:t]]) if params[:t].present?
    return if sql.empty?
    "WHERE #{sql.join(' AND ')}"
  end

  def self.join_subplays(params)
    return unless sub_select_plays_by_played_at_time?(params)
    <<~SQL
      JOIN (
         SELECT
            artists.id,
            sum(_a.num_plays) as sub_num_plays,
            min(_a.first_played) as sub_first_played,
            max(_a.last_played) as sub_last_played,
            sum(_a.length * _a.num_plays) as sub_time_played
          FROM artists
          JOIN (
            SELECT recordings.id,
              recordings.length,
              ra.artist_id,
              count(plays) as num_plays,
              min(plays.played_at#{at_time_zone}) as first_played,
              max(plays.played_at#{at_time_zone}) as last_played
            FROM recordings
            JOIN recording_artists ra ON ra.recording_id = recordings.id
            LEFT JOIN plays ON plays.recording_id = recordings.id
              AND #{filter_time_played_at(params)}
            GROUP BY recordings.id, ra.artist_id
          ) as _a ON _a.artist_id = artists.id
        GROUP BY artists.id
      ) AS subplays ON subplays.id = artists.id
    SQL
  end
end
