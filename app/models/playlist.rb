class Playlist < ApplicationRecord
  has_many :playlist_recordings
  has_many :recordings, through: :playlist_recordings

  validates_presence_of :name
  validates_uniqueness_of :name

  scope :for_index, -> {
    select(<<~SQL
      playlists.*,
      mode() within group (order by recordings.category) AS category
    SQL
    ).joins(<<~SQL
      INNER JOIN playlist_recordings pr ON pr.playlist_id = playlists.id
      INNER JOIN recordings             ON recordings.id  = pr.recording_id
    SQL
    ).group('playlists.id').from('playlists')
  }

  def length
    secs = recordings.sum(&:length) / 1000
    [[60, :seconds], [60, :minutes], [24, :hours], [1000, :days]].map{ |count, name|
      if secs > 0
        secs, n = secs.divmod(count)
        "#{n.to_i} #{name}"
      end
    }.compact.reverse.join(' ')
  end
end
