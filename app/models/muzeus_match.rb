class MuzeusMatch < ApplicationRecord
  has_many :match_recordings, dependent: :destroy
  has_many :recordings, through: :match_recordings

  validates_uniqueness_of :name
  validates_presence_of :name, :recording_string, :artist_string

  REMOVE_PATTERNS = [
    /(\([^\)]*remaster.*\))/,
    /(\/[^-]*remaster.*)|(-[^\/]*remaster.*)/,
    /(\([^\)]*edition.*\))/,
    /(\([^\)]*version.*\))/,
    /(\s)/
  ].freeze

  REPLACE_PATTERNS = [
    { find: /&/, replace: 'and'}
  ]

  def self.encode(recording_string:, release_string:, artist_string:)
    art, rec, rel = artist_string.dup, recording_string.dup, release_string.dup

    [art, rec, rel].each do |string|
      string.downcase!
      REPLACE_PATTERNS.each { |pattern| string.gsub!(pattern[:find], pattern[:replace]) }
      REMOVE_PATTERNS.each { |pattern| string.gsub!(pattern, '') }
    end.join
  end
end
