class Release < ApplicationRecord
  enum category: CATEGORIES
  enum status: STATUSES

  has_many :release_artists, dependent: :destroy
  has_many :artists, through: :release_artists

  has_many :release_recordings, dependent: :destroy
  has_many :recordings, through: :release_recordings

  has_many :plays

  validates_presence_of :name, :mbid

  def self.rank_column
    'time_played'
  end

  def self.icon
    "<i class='fa-duotone fa-album-collection'></i>"
  end

  def status_icon
    Release.status_icons(status)
  end

  def self.ranked(params)
    find_by_sql(<<~SQL
      SELECT
        releases.*,
        played_releases.num_plays,
        played_releases.time_played,
        played_releases.first_played,
        played_releases.last_played,
        played_releases.overall_rank,
        played_releases.t_value
        #{get_subrank(params)}
        #{played_at_subselects('releases', params)}
        #{artist_subselects}
      FROM releases
      LEFT JOIN played_releases ON releases.id = played_releases.release_id
      #{join_subplays(params)}
      #{join_artists('release')}
      #{set_filters(params)}
      #{set_order('releases', params)}
      #{set_limit(params)}
    SQL
    )
  end

  def self.set_filters(params)
    return "WHERE #{sanitize_sql(['releases.id=?', params[:id]])}" if params[:id].present?
    sql = []
    sql << filter_artists(params) if params[:artist_id].present?
    sql << filter_enum(Release, 'category', params[:category]) if params[:category].present?
    sql << filter_enum(Release, 'status', params[:status]) if params[:status].present?
    sql << filter_year(params) if params[:year].present?
    sql << "sub_num_plays > 0" if sub_select_plays_by_played_at_time?(params)
    sql << Release.sanitize_sql(["overall_rank >= ?", params[:rank_min]]) if params[:rank_min].present?
    sql << Release.sanitize_sql(["overall_rank <= ?", params[:rank_max]]) if params[:rank_max].present?
    sql << Release.sanitize_sql(["t_value = ?", params[:t]]) if params[:t].present?
    return if sql.empty?
    "WHERE #{sql.join(' AND ')}"
  end

  def self.join_subplays(params)
    return unless sub_select_plays_by_played_at_time?(params)
    <<~SQL
      JOIN (
         SELECT
            releases.id,
            sum(_r.num_plays) as sub_num_plays,
            min(_r.first_played) as sub_first_played,
            max(_r.last_played) as sub_last_played,
            sum(_r.length * _r.num_plays) as sub_time_played
          FROM releases
          JOIN (
            SELECT recordings.id,
              recordings.length,
              rr.release_id,
              count(plays) as num_plays,
              min(plays.played_at#{at_time_zone}) as first_played,
              max(plays.played_at#{at_time_zone}) as last_played
            FROM recordings
            JOIN release_recordings rr ON rr.recording_id = recordings.id
            LEFT JOIN plays ON plays.recording_id = recordings.id
              AND #{filter_time_played_at(params)}
            GROUP BY recordings.id, rr.release_id
          ) as _r ON _r.release_id = releases.id
        GROUP BY releases.id
      ) AS subplays ON subplays.id = releases.id
    SQL
  end
end
