class Play < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :recording
  belongs_to :release

  validates_uniqueness_of :played_at
  validates_presence_of :recording, :played_at

  def self.icon
    "<i class='fa-duotone fa-circle-play'></i>"
  end

  def self.latest_play_utc
    Play.order("played_at").last.played_at.to_i
  end

  def self.ranked(query_params={})
    sql = <<-SQL.strip_heredoc
      SELECT plays.id,
        plays.played_at#{at_time_zone} as played_at,
        plays.user_id,
        played_releases.overall_rank as release_rank,
        played_releases.t_value as release_t_value,
        played_recordings.num_plays,
        releases.id           as release_id,
        releases.status       AS status,
        releases.year         AS year,
        releases.name         AS release_name,
        recordings.length,
        recordings.name       AS name,
        recordings.id         AS recording_id,
        recordings.category   AS category
        #{artist_subselects}
      FROM plays
      JOIN played_releases    ON played_releases.release_id = plays.release_id
      JOIN releases           ON releases.id = plays.release_id
      JOIN played_recordings  ON played_recordings.recording_id = plays.recording_id
      JOIN recordings         ON recordings.id = plays.recording_id
      JOIN joined_recording_artists AS _artists ON _artists.recording_id = plays.recording_id
      #{filter(query_params)}
      #{set_order('plays', query_params)}
    SQL
    sql += "LIMIT #{query_params[:limit]} " if query_params[:limit]
    sql += "OFFSET #{query_params[:offset]} " if query_params[:offset]
    find_by_sql(sql)
  end

  def self.filter(params)
    sql = []
    sql << filter_artists(params) if params[:artist_id].present?
    sql << filter_enum(Recording, 'category', params[:category], 'recordings.category') if params[:category].present?
    sql << filter_enum(Release, 'status', params[:status], 'releases.status') if params[:status].present?
    sql << filter_year(params) if params[:year].present?
    sql << filter_by_date(params) if params[:date].present?
    sql << filter_time_played_at(params) if %i(play_year play_month play_day play_dow play_hour).any? { |k| params.keys.include?(k) }
    sql << Play.sanitize_sql(["played_releases.t_value = ?", params[:t]]) if params[:t].present?
    sql = [Play.sanitize_sql(["plays.release_id = ?", params[:release_id]])] if params[:release_id].present?
    sql = [Play.sanitize_sql(["plays.recording_id = ?", params[:recording_id]])] if params[:recording_id].present?
    return if sql.empty?
    "WHERE #{sql.join(' AND ')}"
  end

  def self.filter_by_date(query_params)
    date       = query_params[:date].to_date
    end_date   = query_params.key?(:end_date) ? query_params[:end_date].to_date : date
    start_date = (date.beginning_of_day + 4.hours).utc
    end_date   = (end_date.end_of_day + 4.hours).utc
    Recording.sanitize_sql(["plays.played_at#{at_time_zone} BETWEEN ? AND ?", start_date, end_date])
  end
end
