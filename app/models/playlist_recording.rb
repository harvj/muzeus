class PlaylistRecording < ApplicationRecord
  belongs_to :playlist
  belongs_to :recording

  validates_presence_of :playlist_id, :recording_id, :position
  validates_uniqueness_of :recording_id, :position, scope: :playlist_id
end
