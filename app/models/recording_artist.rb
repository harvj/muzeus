class RecordingArtist < ApplicationRecord
  belongs_to :recording
  belongs_to :artist

  validates :artist_id, uniqueness: { scope: :recording_id }
end
