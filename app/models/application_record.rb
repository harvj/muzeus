class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  PLAYED_AT_QUERY_KEYS    = %i(play_year play_month play_day play_hour play_dow).freeze
  COMBINABLE_QUERY_KEYS = %i(artist_id category status year).concat(PLAYED_AT_QUERY_KEYS).freeze
  ACCEPTED_QUERY_KEYS = %i(
    date
    length_max
    length_min
    name
    order
    page
    plays
    plays_end
    rank
    rank_max
    rank_min
    release_id
    t
  ).concat(COMBINABLE_QUERY_KEYS).freeze

  CATEGORIES = { black: 0, brown: 1,  salmon: 2,  pink: 3, orange: 4,
    yellow: 5,  green: 6, forest: 7, blue: 8, purple: 9, grey: 10,  ground: 11
  }.freeze

  STATUSES = {
    shelf: 0,
    hot: 1,
    rotation: 2,
    rising: 3,
    explore: 4
  }.freeze

  def self.status_icons(key)
    icons = {
      'explore' => 'fad fa-compass',
      'hot' => 'fa-duotone fa-fire',
      'rising' => 'fa-solid fa-square-arrow-up',
      'rotation' => 'fa-solid fa-stars'
    }
    icons[key] || 'fa-duotone fa-square'
  end

  def self.at_time_zone(zone='America/Chicago')
    "::TIMESTAMP at time zone 'UTC' at time zone '#{zone}'"
  end

  def self.filter_artists(params)
    return if params[:artist_id].blank?
    query_array = params[:artist_id].to_s.split(',').map(&:to_i)
    sanitize_sql(["artist_ids && ARRAY[?]", query_array])
  end

  def self.filter_enum(klass, key, param, column_name=nil)
    column_name = column_name || key
    param_array = klass.send(key.pluralize).slice(*param.split(',')).values
    sanitize_sql(["#{column_name} IN (?)", param_array])
  end

  def self.filter_year(params, column_name='year')
    return if params[:year].blank?
    year_array = params[:year].split(',').map(&:to_i)
    sanitize_sql(["#{column_name} IN (?)", year_array])
  end

  def self.filter_time_played_at(params)
    result = []
    %i(year month day dow hour).each do |key|
      param = "play_#{key}".to_sym
      next if params[param].nil?
      value = params[param].split(',').map(&:to_i)
      result << sanitize_sql([" EXTRACT(#{key} FROM plays.played_at#{at_time_zone('America/Los_Angeles')}) IN (?)", value])
    end
    return if result.empty?
    result.join(' AND')
  end

  def self.get_subrank(params)
    attr_name = sub_select_plays_by_played_at_time?(params) ? "sub_#{rank_column}" : rank_column
    <<~SQL
      ,( CASE WHEN #{attr_name} IS NOT NULL
        THEN rank() OVER (PARTITION BY #{attr_name} IS NOT NULL ORDER BY #{attr_name} DESC)
        ELSE rank() OVER (ORDER BY #{attr_name} ASC)
        END
      ) as subrank
    SQL
  end

  def self.sub_select_plays_by_played_at_time?(params)
    PLAYED_AT_QUERY_KEYS.any? { |key| params[key].present? }
  end

  def self.played_at_subselects(key, params)
    return unless sub_select_plays_by_played_at_time?(params)
    sql = <<~SQL
      ,subplays.sub_num_plays
      ,subplays.sub_first_played
      ,subplays.sub_last_played
    SQL
    sql += ',subplays.sub_time_played' if %w(artists releases).include?(key)
    sql
  end

  def self.artist_subselects
    <<~SQL
      ,_artists.artist_ids
      ,_artists.artist_name
      ,_artists.json_artists
    SQL
  end

  def self.join_artists(key)
    <<~SQL
      JOIN joined_#{key}_artists as _artists ON #{key.pluralize}.id = _artists.#{key}_id
    SQL
  end

  def self.release_subselects
    <<~SQL
      ,_releases.release_name
      ,_releases.release_ids
      ,_releases.json_releases
      ,_releases.year
    SQL
  end

  def self.join_releases
    <<~SQL
      JOIN joined_recording_releases as _releases ON recordings.id = _releases.recording_id
    SQL
  end

  def self.set_order(key, params)
    params[:order] = params[:order].gsub(/\-in\-group/, '') if params[:order] && !sub_select_plays_by_played_at_time?(params)
    params[:order] = 'most-played-in-group' if sub_select_plays_by_played_at_time?(params) && params[:order].blank?
    params[:order] = 'recently-played' unless params[:order].present?
    <<~SQL
      ORDER BY #{order_translations(key, params[:order])}
    SQL
  end

  def self.set_limit(params)
    return unless params[:limit].present?
    <<~SQL
      LIMIT #{params[:limit]}
    SQL
  end

  def self.order_translations(scope, key)
    play_datetime = scope == 'plays' ? 'played_at' : 'last_played'
    play_amount = scope == 'recordings' ? 'num_plays' : 'time_played'
    translations = {
      'artist-name-a' => "artist_name asc, #{play_datetime} desc",
      'artist-name-z' => "artist_name desc, #{play_datetime} desc",
      'category-a' => "category asc, #{play_datetime} desc",
      'category-z' => "category desc, #{play_datetime} desc",
      'recently-discovered' => 'first_played desc nulls last',
      'not-recently-discovered' => 'first_played asc nulls last',
      'shortest' => 'length asc',
      'longest' => 'length desc',
      'name-a' => 'name asc',
      'name-z' => 'name desc',
      'most-played' => "#{play_amount} desc nulls last, #{play_datetime} desc",
      'least-played' => "#{play_amount} asc nulls first, #{play_datetime} desc",
      'recently-played' => "#{play_datetime} desc nulls last",
      'not-recently-played' => "#{play_datetime} asc nulls last",
      'hot' => 'status desc',
      'cold' => 'status asc',
      'recently-discovered-in-group' => 'sub_first_played desc',
      'not-recently-discovered-in-group' => 'sub_first_played asc',
      'recently-played-in-group' => 'sub_last_played desc nulls last',
      'not-recently-played-in-group' => 'sub_last_played asc',
      'most-played-in-group' => "sub_#{play_amount} desc nulls last, #{play_datetime} desc",
      'least-played-in-group' => "sub_#{play_amount} asc nulls first, #{play_datetime} desc",
      'recent-release' => 'year desc, month desc, day desc, category asc',
      'not-recent-release' => 'year asc, month asc, day asc, category asc',
      'overall-rank-1' => 'overall_rank asc',
      'overall-rank-9' => 'overall_rank desc',
    }
    translations[key]
  end
end
