class Recording < ApplicationRecord
  enum category: CATEGORIES
  enum status: { shelf: 0 }

  belongs_to :artist, optional: true

  has_many :release_recordings, dependent: :destroy
  has_many :releases, through: :release_recordings

  has_many :recording_artists, dependent: :destroy
  has_many :artists, through: :recording_artists

  has_many :match_recordings, dependent: :destroy
  has_many :muzeus_matches, through: :match_recordings

  has_many :plays

  validates_presence_of :name, :mbid
  validates_uniqueness_of :mbid

  def self.rank_column
    'num_plays'
  end

  def self.icon
    "<i class='fa-duotone fa-file-music'></i>"
  end

  scope :for_release, ->(id) {
    find_by_sql(<<~SQL
      SELECT recordings.id,
        recordings.name,
        recordings.length,
        recordings.category,
        count(plays.recording_id) as num_plays,
        max(rr.position) as position,
        (array_agg(ja.json_artists))[1] as json_artists,
        max(plays.played_at#{at_time_zone}) as last_played,
        min(plays.played_at#{at_time_zone}) as first_played
      FROM recordings
      LEFT JOIN plays ON plays.recording_id = recordings.id
      LEFT JOIN release_recordings rr ON rr.recording_id = recordings.id
      JOIN (
        SELECT recordings.id,
        json_agg(
          json_build_object(
            'id', artists.id,
            'name', artists.name
          )
          ORDER BY ra.cardinality) AS json_artists
        FROM recordings
        JOIN recording_artists ra ON ra.recording_id = recordings.id
        JOIN artists ON ra.artist_id = artists.id
        GROUP BY recordings.id
      ) as ja ON recordings.id = ja.id
      WHERE rr.release_id = #{id}
      GROUP BY recordings.id
      ORDER BY position
    SQL
    )
  }

  scope :for_playlist, ->(id) {
    select(<<~SQL
      recordings.*,
      count(plays.recording_id) AS num_plays,
      max(pr.position)          AS pos,
      max(artists.name)         AS artist_name,
      max(artists.id)           AS artist_id
    SQL
    ).joins(<<~SQL
      LEFT JOIN plays                   ON plays.recording_id = recordings.id
      LEFT JOIN playlist_recordings pr  ON pr.recording_id = recordings.id
      INNER JOIN artists                ON recordings.artist_id = artists.id
    SQL
    ).where('pr.playlist_id = ?', id).from('recordings').order('pos').group('recordings.id')
  }

  def self.display_length(length)
    return '-' if length.nil?
    secs = length / 1000
    sec = (secs % 60).to_s.rjust(2,'0')
    min = secs / 60
    [min,sec].join(':')
  end

  def display_length
    self.class.display_length(self.length)
  end

  def self.ranked(params)
    find_by_sql(<<~SQL
      SELECT
        recordings.*,
        0 as status,
        played_recordings.overall_rank as overall_rank,
        played_recordings.num_plays as num_plays,
        played_recordings.first_played as first_played,
        played_recordings.last_played as last_played
        #{get_subrank(params)}
        #{played_at_subselects('recording', params)}
        #{artist_subselects}
        #{release_subselects}
      FROM recordings
      JOIN played_recordings ON played_recordings.recording_id = recordings.id
      #{join_subplays(params)}
      #{join_artists('recording')}
      #{join_releases}
      #{filter(params)}
      #{set_order('recordings', params)}
      #{set_limit(params)}
    SQL
    )
  end

  def self.join_subplays(params)
    return unless sub_select_plays_by_played_at_time?(params)
    <<~SQL
      JOIN (
        SELECT recordings.id,
          count(plays) as sub_num_plays,
          min(plays.played_at#{at_time_zone}) as sub_first_played,
          max(plays.played_at#{at_time_zone}) as sub_last_played
        FROM recordings
        LEFT JOIN plays ON plays.recording_id = recordings.id
          AND #{filter_time_played_at(params)}
        GROUP BY recordings.id
      ) as subplays ON subplays.id = recordings.id
    SQL
  end

  def self.filter(params)
    return "WHERE #{Recording.sanitize_sql(['id = ?', params[:id]])}" if params[:id].present?
    sql = []
    if params[:plays].present?
      sql << Recording.sanitize_sql(['num_plays = ?', params[:plays]]) if params[:plays].to_i < 63
      sql << Recording.sanitize_sql(['num_plays >= 63 AND num_plays < 70']) if params[:plays].to_i >= 63 && params[:plays].to_i < 70
      sql << Recording.sanitize_sql(['num_plays >= 70 AND num_plays < 80']) if params[:plays].to_i >= 70 && params[:plays].to_i < 80
      sql << Recording.sanitize_sql(['num_plays >= 80 AND num_plays < 90']) if params[:plays].to_i >= 80 && params[:plays].to_i < 90
      sql << Recording.sanitize_sql(['num_plays >= 90 AND num_plays < 100']) if params[:plays].to_i >= 90 && params[:plays].to_i < 100
      sql << Recording.sanitize_sql(['num_plays >= 100']) if params[:plays].to_i >= 100
    end
    sql << filter_artists(params) if params[:artist_id].present?
    sql << filter_enum(Recording, 'status', params[:status]) if params[:status].present?
    sql << filter_enum(Recording, 'category', params[:category]) if params[:category].present?
    sql << filter_year(params) if params[:year].present?
    sql << Recording.sanitize_sql(["?=ANY(release_ids)", params[:release_id]]) if params[:release_id].present?
    sql << Recording.sanitize_sql(["overall_rank = ?", params[:rank]]) if params[:rank].present?
    sql << Recording.sanitize_sql(["LOWER(name) LIKE LOWER(?)", "%#{params[:name]}%"]) if params[:name].present?
    if params[:length_min].present?
      min = params[:length_min][0..-3]
      sec = params[:length_min][-2..-1]
      min_length = (min.to_i * 60000) + (sec.to_i * 1000)
      sql << Recording.sanitize_sql(["length >= ?", min_length])
    end
    if params[:length_max]
      min = params[:length_max][0..-3]
      sec = params[:length_max][-2..-1]
      max_length = (min.to_i * 60000) + (sec.to_i * 1000) + 999
      sql << Recording.sanitize_sql(["length <= ?", max_length])
    end
    sql << "sub_num_plays > 0" if sub_select_plays_by_played_at_time?(params)
    return if sql.empty?
    "WHERE #{sql.join(' AND ')}"
  end
end
