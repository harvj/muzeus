module Connectors
  Response = ImmutableStruct.new(:results, :page, :per_page, :total_pages, [:errors]) do
    def success?
      errors.empty?
    end
  end

  class Base
    include ServiceObject

    def initialize
      @body   = {}
      @errors = []
    end

    def config
      @config ||= Rails.application.secrets.send(self.class.name.demodulize.underscore).symbolize_keys
    end
  end
end
