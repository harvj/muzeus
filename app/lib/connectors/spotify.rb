module Connectors
  class Spotify < Connectors::Base
    def self.get(endpoint, user)
      self.new(user).send :connect, endpoint
    end

    def initialize(user)
      @user = user
    end

    attr_reader :user

    private

    def connect(endpoint)
      refresh! if expired?
      send(endpoint)
    end

    def playlists
      url = config[:base_api] + "users/#{user.uid}/playlists"
      results = []
      more = true
      while more
        response = HTTP.headers(auth_header).get(url)
        response = JSON.parse(response.body)
        results += response['items'].map do |i|
          {
            id: i['id'],
            name: i['name'],
            track_count: i['tracks']['total']
          }
        end
        url = response['next']
        more = url.present?
      end
      results
    end

    def expired?
      Time.now > user.spotify_token_expires_at
    end

    def refresh!
      params = auth_params.merge(
        grant_type: 'refresh_token',
        refresh_token: user.spotify_refresh_token
      )
      response = HTTP.post(config[:base_url] + 'api/token', form: params)
      if response.status.success?
        response = JSON.parse(response.body)
        user.update_attributes(
          spotify_access_token: response['access_token'],
          spotify_token_expires_at: Time.now + 1.hour
        )
        user.reload
      else
        log response.body
      end
    end

    def auth_header
      {
        'Authorization' => "Bearer #{user.spotify_access_token}"
      }
    end

    def auth_params
      {
        client_id: Rails.application.credentials.spotify[:client_id],
        client_secret: Rails.application.credentials.spotify[:client_secret]
      }
    end
  end
end
