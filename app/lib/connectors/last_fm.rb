module Connectors
  class LastFm < Connectors::Base

    def earliest_page
      connect('user.getrecenttracks', 'recenttracks', from: Play.latest_play_utc + 1)
      metadata['totalPages']
    end

    def next_play
      connect('user.getrecenttracks', 'recenttracks', page: earliest_page)
      Connectors::Response.new(
        results:     @results ? map_play(@results['track'].last) : nil,
        page:        metadata["page"],
        per_page:    metadata["perPage"],
        total_pages: metadata["totalPages"],
        errors: @errors
      )
    end

    private

    def connect(api_method, results_key, options={})
      response = HTTP.get(config[:base_url], params: params(api_method, options))
      @results = JSON.parse(response)[results_key]
      error(response.inspect) unless @results.present?
      @results
    end

    def metadata
      # binding.pry unless @results
      # return {} unless @results
      @results["@attr"]
    end

    def params(method, options={})
      {
        method: method,
        format: 'json',
        api_key: config[:api_key],
        user: options[:user] || 'qalephd',
        page: options[:page] || 1,
        limit: options[:per_page] || 1,
        from: (options[:from] || DateTime.parse('2000-01-01')).to_i
      }
    end

    def map_play(play)
      {
        recording_string: play['name'],
        release_string:   play['album']['#text'],
        artist_string:    play['artist']['#text'],
        time:             DateTime.parse(play['date']['#text']).in_time_zone,
        utc:              play['date']['uts'],
        artist_mbid:      play['artist']['mbid']
      }
    end

  end
end
