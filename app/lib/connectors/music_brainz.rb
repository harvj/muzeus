module Connectors
  class MusicBrainz < Connectors::Base

    def query_recording(params)
      query(:recording, params.merge(album_params))
    end

    def lookup_release(mbid)
      lookup(:release, mbid, :recordings, :artists, 'artist-credits')
    end

    private

    def query(entity, params)
      get("#{base_url}#{entity}/#{formatted_query_params(params)}")
    end

    def lookup(entity, mbid, *includes)
      get("#{base_url}#{entity}/#{mbid}#{lookup_includes(includes)}")
    end

    def formatted_query_params(params)
      "?query=#{url_encode(params.delete(:query))}#{lucene_formatted(params)}"
    end

    def lookup_includes(includes)
      "?inc=#{includes.join('+')}"
    end

    def album_params
      {
        status: 'official',
        type: 'album'
      }
    end

    def single_params
      {
        status: 'official',
        primarytype: 'single'
      }
    end

    def comp_params
      {
        status: 'official',
        secondarytype: 'compilation'
      }
    end

    def sound_params
      {
        status: 'official',
        secondarytype: 'soundtrack'
      }
    end

    # ----- Util

    URL_SPACE  = '%22'
    LUCENE_AND = '%20AND%20'

    def lucene_formatted(params)
      LUCENE_AND + params.map{ |k,v| "#{k}:#{url_encode(v)}" }.join(LUCENE_AND)
    end

    def url_encode(input)
      URL_SPACE + ERB::Util.url_encode(input) + URL_SPACE
    end

    def get(url)
      HTTP.headers(headers).get(url)
    end

    # ----- Config

    def headers
      { user_agent: config[:user_agent] }
    end

    def base_url
      config[:base_url]
    end
  end
end
