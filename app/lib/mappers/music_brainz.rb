module Mappers
  class MusicBrainz < Mappers::Base

    attr_reader :release

    def get_mbids
      as_mapped_response { mbids }
    end

    def map_release
      as_mapped_response { release_to_json }
    end

    private

    def query_results_to_json
      Nokogiri::XML(data).css('recording-list > recording').collect do |recording|
        {
          name:         recording.css('> title').text,
          length:       recording.css('> length').text.to_i,
          mbid:         recording.attribute('id').value,
          artist_list:  recording.css('> artist-credit > name-credit > artist').collect { |node| map_artist(node) },
          release_list: recording.css('> release-list > release').collect               { |node| map_recording_release(node) }
        }
      end
    end

    def release_to_json
      Nokogiri::XML(data).css('release').collect do |release|
        @release = release
        {
          name: release.css('> title').text,
          year: release.css('> date').text.split('-')[0],
          month: release.css('> date').text.split('-')[1],
          day: release.css('> date').text.split('-')[2],
          mbid: release.attribute('id').value,
          artist_list: release.css('> artist-credit > name-credit > artist').collect { |node| map_artist(node) },
          recording_list: map_recording_list(release)
        }
      end
    end

    def map_artist(artist)
      {
        name: artist.css('> name').text,
        mbid: artist.attribute('id').value
      }
    end

    def map_recording_list(release)
      recordings = []
      release.css('> medium-list > medium > track-list > track').each_with_index do |recording, index|
        recordings << {
          position: index+1,
          name: recording.css('> recording > title').text,
          length: recording.css('> recording > length').text,
          mbid: recording.css('> recording').attribute('id').value,
          artist_list: recording.css('recording > artist-credit > name-credit > artist').collect { |node| map_artist(node) }
        }
      end
      recordings
    end

    def map_recording(node)

    end

    def map_recording_release(release)
      {
        name:         release.css('> title').text,
        status:       release.css('> status').text,
        type:        [release.css('> release-group').attribute('type').try(:value), release.css('> release-group > primary-type').text].uniq.join(' '),
        date:         release.css('> date').text,
        country:      release.css('> country').text,
        position:     release.css('> medium-list > medium > track-list > track > number').text,
        format:       release.css('> medium-list > medium > format').text,
        release_mbid: release.attribute('id').value,
        release_group_mbid: release.css('> release-group').attribute('id').value
      }
    end

    def release_key(release_data)
      {
        name:   release_data[:name],
        status: release_data[:status],
        type:   release_data[:type],
        release_group_mbid: release_data[:release_group_mbid]
      }
    end

    def recording_for_release(release_data, recording_data)
      {
        name:     recording_data[:name],
        mbid:     recording_data[:mbid],
        length:   recording_data[:length],
        position: release_data[:position],
        format:   release_data[:format],
        date:     sortable_date(release_data[:date]),
        country:  release_data[:country],
        release_mbid: release_data[:release_mbid],
      }
    end

    def mbids
      json = query_results_reordered[0]

      artist = json[:artists][0]
      release = json[:releases][0]

      if a = Artist.find_by(mbid: artist[:mbid])
        rels = a.releases.pluck(:mbid)
        recording = release[:recordings].select{|r| rels.include?(r[:release_mbid])}.first
        recording = release[:recordings][0] if recording.nil?
      else
        recording = release[:recordings][0]
      end

      {
        artist: artist[:mbid],
        release: recording[:release_mbid],
        recording: recording[:mbid]
      }
    end

    # ------ Util

    def query_results_by_artist
      query_results_to_json.group_by{|i| i.delete(:artist_list)}
    end

    def query_results_reordered
      reversed = query_results_by_artist.inject({}) do |result, (artist_key, recordings)|
        releases = Hash.new{|h,k| h[k]=[]}

        recordings.each do |rec|
          rec[:release_list].each do |rel|
            releases[release_key(rel)] << recording_for_release(rel,rec)
          end
        end

        result[artist_key] = releases
        result
      end

      reversed.collect do |artists, releases|
        {
          artists: artists.collect do |a|
            {
              name: a[:name],
              mbid: a[:mbid],
            }
          end,
          releases: releases.collect do |rel,recordings|
            qual = qualified_recordings(recordings)
            next if qual.empty?
            {
              name:               rel[:name],
              status:             rel[:status],
              type:               rel[:type],
              release_group_mbid: rel[:release_group_mbid],
              earliest_date:      qual.first[:date],
              recordings:         qual
            }
          end.compact.sort_by{|rel| rel[:earliest_date]}
        }
      end
    end

    def qualified_recordings(recordings)
      # recordings = recordings.select  { |rec| COUNTRIES.include?(rec[:country]) }#|| Release.find_by(mbid: rec[:release_mbid]).present? }
      recordings = recordings.sort_by { |rec| rec[:date] }
    end

    def sortable_date(date)
      year, month, day = date.split('-')
      if year.nil?
        '99999'
      elsif month.nil?
        [year, '13', '01'].join('-')
      elsif day.nil?
        [year, month, '32'].join('-')
      else
        date
      end
    end

    COUNTRIES = ['US', 'GB', 'CA', 'NO']

  end
end
