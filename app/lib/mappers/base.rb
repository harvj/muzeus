module Mappers
  Response = ImmutableStruct.new(:results, [:errors]) do
    def success?
      errors.empty?
    end
  end

  class Base
    include ServiceObject

    attr_reader :data
    attr_accessor :results, :errors

    def initialize(data, options={})
      @data    = data
      @results = {}
      @errors  = []
    end

    def as_mapped_response
      results = yield
      Mappers::Response.new(results: results, errors: errors)
    end
  end
end
