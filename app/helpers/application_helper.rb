module ApplicationHelper
  def time_in_hours(milliseconds)
    return 0 if milliseconds.nil?
    number_with_precision(milliseconds/60000.0, precision: 1)
  end
end
