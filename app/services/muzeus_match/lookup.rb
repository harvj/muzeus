class MuzeusMatch::Lookup
  include ServiceObject

  attr_accessor :name, :params

  def initialize(params)
    @params = params
    calculate_name
  end

  def call
    match = MuzeusMatch.find_by(name: name) || MuzeusMatch::Create.(params.merge(name: name)).result
    Services::Response.new(result: match.recordings, errors: @errors)
  end

  private

  def calculate_name
    self.name = MuzeusMatch.encode(params)
  end
end
