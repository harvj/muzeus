class MuzeusMatch::Create < Services::Create
  def apply_pre_processing
    @query_recording = MusicBrainz::QueryRecording.(
      query: params[:recording_string],
      artist: params[:artist_string],
      release: params[:release_string]
    )

    @import_release  = MusicBrainz::ImportRelease.(@query_recording.results[:release])

    @errors = @import_release.errors
  end

  def apply_post_processing
    recording = Recording.find_by(mbid: @query_recording.results[:recording])
    if recording.present?
      MatchRecording.where(recording_id: recording.id, muzeus_match_id: @result.id).first_or_create
    end
  end
end
