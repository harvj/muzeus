module Services
  Response = ImmutableStruct.new(:result, [:errors]) do
    def success?
      errors.empty?
    end
  end

  # ----- Services::Base

  class Base
    include ::ServiceObject

    attr_reader :params

    def target_class
      target_model_name.constantize
    end

    def target_model_name
      self.class.name.deconstantize
    end

    def apply_pre_processing; end
    def apply_post_processing; end
  end

  # ----- Services::Build

  class Build < Services::Base
    def initialize(params={})
      @subject = target_class.new(params)
    end

    def call
      apply_pre_processing
      @subject
    end
  end

  # ----- Services::Create

  class Create < Services::Base
    def initialize(params={})
      @params = params
      @result = target_class::Build.(params)
    end

    attr_reader :params, :result

    def call
      create { result.save! }
    end

    private

    def create(&block)
      apply_pre_processing
      if ok?
        apply_post_processing if yield
      end
      Services::Response.new(result: result, errors: result.errors)
    end
  end

  # ----- Services::Update

  class Update < Services::Base
    def self.call(subject, params={})
      self.new(subject, params).call
    end

    def initialize(subject, params={})
      @subject = subject
      @params = params
    end

    def call
      update { @subject.update_attributes!(params) }
    end

    private

    attr_reader :subject, :params

    def update(&block)
      apply_pre_processing
      if ok?
        apply_post_processing if changed? && yield
      end
      Services::Response.new(result: @subject, errors: @subject.errors)
    end

    def changed?
      @subject.attributes != @subject.attributes.with_indifferent_access.merge(params)
    end
  end

  class Import < Services::Base
    def initialize(params={})
      @params = params
      @result = target_class.find_by(mbid: @params[:mbid])
    end

    def call
      if @result.present?
        target_class::Update.(@result, params)
      else
        target_class::Create.(params)
      end
    end
  end
end
