module ServiceObject
  include SimpleLog

  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def call(*args)
      new(*args).()
    end
  end

  def error(message=nil)
    message = 'default' if message.nil?
    @errors = [] unless defined?(@errors)
    @errors << message
  end
  alias_method :no, :error

  def ok?
    @errors.blank?
  end
end
