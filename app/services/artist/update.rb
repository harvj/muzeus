class Artist::Update < Services::Update
  def apply_pre_processing
    if cascade_categories?
      subject.releases.update_all(category: params[:category])
      subject.recordings.update_all(category: params[:category])
    end
  end

  private

  def cascade_categories?
    ActiveModel::Type::Boolean.new.cast(params.delete(:cascade_categories))
  end
end
