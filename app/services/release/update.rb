class Release::Update < Services::Update
  def apply_pre_processing
    subject.recordings.update_all(category: params[:category]) if cascade_categories?
  end

  private

  def cascade_categories?
    ActiveModel::Type::Boolean.new.cast(params.delete(:cascade_categories))
  end
end
