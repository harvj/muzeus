class MusicBrainz::ImportRelease
  include ServiceObject

  def initialize(mbid, options={})
    @mbid              = mbid
    @options           = options
    @release_artists   = []
    @recordings        = []
    @recording_artists = []
  end

  attr_reader :mbid, :options, :mb_release, :release, :release_artists, :recordings, :recording_artists, :errors

  def call
    get_release
    import_artists
    import_release
    import_recordings
    ApplicationRecord.connection.execute("REFRESH MATERIALIZED VIEW joined_recording_releases")
    ApplicationRecord.connection.execute("REFRESH MATERIALIZED VIEW joined_recording_artists")
    ApplicationRecord.connection.execute("REFRESH MATERIALIZED VIEW joined_release_artists")
    ApplicationRecord.connection.execute("REFRESH MATERIALIZED VIEW played_recordings")
    ApplicationRecord.connection.execute("REFRESH MATERIALIZED VIEW played_releases")
    ApplicationRecord.connection.execute("REFRESH MATERIALIZED VIEW played_artists")
    Services::Response.new(result: release.try(:result), errors: errors)
  end

  private

  def get_release
    @mb_release = MusicBrainz::GetRelease.(mbid).results.first
  end

  def import_artists
    mb_release[:artist_list].each do |params|
      @release_artists << Artist::Import.(params)
    end
  end

  def import_release
    @release = Release::Import.(release_params)

    release_artists.each_with_index do |artist, index|
      if artist.success?
        ra = ReleaseArtist.where(release_id: release.result.id, artist_id: artist.result.id).first_or_create
        ra.update_attribute(:cardinality, index)
      end
    end
  end

  def import_recordings
    mb_release[:recording_list].each do |params|
      params[:name] = params[:name][0..254] if params[:name].length > 255

      params[:artist_list].each do |artist_params|
        @recording_artists << Artist::Import.(artist_params)
      end
      recording = Recording::Import.(recording_params(params))
      @recordings << recording

      release_recording = ReleaseRecording.where(release_id: release.result.id, recording_id: recording.result.id).first_or_create
      release_recording.update_attribute(:position, params[:position])

      recording_artists.each_with_index do |artist, index|
        if artist.success?
          ra = RecordingArtist.where(recording_id: recording.result.id, artist_id: artist.result.id).first_or_create
          ra.update_attribute(:cardinality, index)
        end
      end
      @recording_artists = []
    end
  end

  def release_params
    first_artist = release_artists.first.result
    mb_release.slice(
      :name,
      :year,
      :month,
      :day,
      :mbid
    ).merge(
      category: first_artist.category
    )
  end

  def recording_params(params)
    first_artist = recording_artists.first.result
    params.slice(
      :name,
      :mbid,
      :length
    ).merge(
      category: first_artist.category
    )
  end
end
