class MusicBrainz::GetRelease
  include ServiceObject

  attr_reader :mbid

  def initialize(mbid)
    @mbid = mbid
  end

  def call
    mb_release_xml = Connectors::MusicBrainz.new.lookup_release(mbid)
    Mappers::MusicBrainz.new(mb_release_xml).map_release
  end
end
