class MusicBrainz::QueryRecording
  include ServiceObject

  attr_reader :query, :artist, :release

  def initialize(query:, artist: nil, release: nil)
    @query  = clean(query)
    @artist = artist
  end

  def call
    mb_query_xml = Connectors::MusicBrainz.new.query_recording(query: query, artist: artist, release: release)
    Mappers::MusicBrainz.new(mb_query_xml).get_mbids
  end

  def clean(query)
    query.gsub!(/\(Remastered\)/,'')
    query.gsub!(/\(\d+ Digital Remaster\)/,'')
    query.strip
  end
end
