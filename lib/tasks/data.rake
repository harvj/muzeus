namespace :data do
  desc "Update recording categories based on release"
  task recording_categories: :environment do
    Release.find_each do |release|
      puts "Updating release #{release.id}" if release.id % 166 == 0
      release.recordings.update_all(category: release.category)
    end
  end
end
