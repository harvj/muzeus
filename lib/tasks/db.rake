namespace :db do
  desc "Remove and recreate materialized views"
  task views: :environment do
    result = ApplicationRecord.connection.execute(
      <<~SQL
        DROP MATERIALIZED view played_releases;
        DROP MATERIALIZED VIEW played_recordings;
        CREATE MATERIALIZED view played_recordings AS
          SELECT
            recordings.id as recording_id,
            recordings.length,
            rr.release_id,
            count(plays) as num_plays,
            min(plays.played_at#{ApplicationRecord.at_time_zone}) as first_played,
            max(plays.played_at#{ApplicationRecord.at_time_zone}) as last_played,
            rank() OVER (ORDER BY count(plays) DESC, recordings.length DESC, max(plays.played_at) DESC) AS overall_rank
          FROM recordings
          JOIN release_recordings rr ON rr.recording_id = recordings.id
          LEFT JOIN plays on recordings.id = plays.recording_id
          GROUP BY recordings.id, rr.release_id
        ;
        CREATE INDEX played_recordings_id ON played_recordings (recording_id)
      SQL
    )
    puts "played_recordings: #{result.cmd_tuples} rows affected"
    result = ApplicationRecord.connection.execute(
      <<~SQL
        CREATE MATERIALIZED view played_releases AS
          WITH _x AS (
            SELECT
              releases.id as release_id,
              sum(played_recordings.num_plays) as num_plays,
              min(played_recordings.first_played) as first_played,
              max(played_recordings.last_played) as last_played,
              sum(played_recordings.length * played_recordings.num_plays) as time_played,
              rank() OVER (
                ORDER BY
                  sum(played_recordings.length * played_recordings.num_plays) DESC nulls last,
                  max(played_recordings.last_played) DESC
                ) AS overall_rank
            FROM releases
            JOIN played_recordings ON played_recordings.release_id = releases.id
            GROUP BY releases.id
          )
          SELECT _x.*,
          (
            CASE WHEN FLOOR((SELECT overall_rank ^ 0.55 / (SELECT (max(overall_rank) * 0.08) ^ 0.55 from _x)) * 10) > 0
            THEN FLOOR((SELECT overall_rank ^ 0.55 / (SELECT (max(overall_rank) * 0.08) ^ 0.55 from _x)) * 10)::int
            ELSE 1
            END
          ) as t_value
          FROM _x
        ;
        CREATE UNIQUE INDEX played_releases_id ON played_releases (release_id)
      SQL
    )
    puts "played_releases: #{result.cmd_tuples} rows affected"
    result = ApplicationRecord.connection.execute(
      <<~SQL
        DROP MATERIALIZED view played_artists;
        CREATE MATERIALIZED view played_artists AS
          WITH _x AS (
            SELECT
              artists.id as artist_id,
              sum(_a.num_plays) as num_plays,
              min(_a.first_played) as first_played,
              max(_a.last_played) as last_played,
              sum(_a.length * _a.num_plays) as time_played,
              rank() OVER (
                ORDER BY
                  sum(_a.length * _a.num_plays) DESC nulls last,
                  max(_a.last_played) DESC
                ) AS overall_rank
            FROM artists
            JOIN (
              SELECT recordings.id,
                recordings.length,
                ra.artist_id,
                count(plays) as num_plays,
                min(plays.played_at#{ApplicationRecord.at_time_zone}) as first_played,
                max(plays.played_at#{ApplicationRecord.at_time_zone}) as last_played,
                rank() OVER (ORDER BY count(plays) DESC, max(plays.played_at) DESC) AS overall_rank
              FROM recordings
              JOIN recording_artists ra ON ra.recording_id = recordings.id
              LEFT JOIN plays on recordings.id = plays.recording_id
              GROUP BY recordings.id, ra.artist_id
            ) as _a ON _a.artist_id = artists.id
            GROUP BY artists.id
          )
          SELECT _x.*,
          (
            CASE WHEN FLOOR((SELECT overall_rank ^ 0.55 / (SELECT (max(overall_rank) * 0.08) ^ 0.55 from _x)) * 10) > 0
            THEN FLOOR((SELECT overall_rank ^ 0.55 / (SELECT (max(overall_rank) * 0.08) ^ 0.55 from _x)) * 10)::int
            ELSE 1
            END
          ) as t_value
          FROM _x
        ;
        CREATE UNIQUE INDEX played_artists_id ON played_artists (artist_id)
      SQL
    )
    puts "played_artists: #{result.cmd_tuples} rows affected"
    result = ApplicationRecord.connection.execute(
      <<~SQL
        DROP MATERIALIZED VIEW joined_recording_releases;
        CREATE MATERIALIZED VIEW joined_recording_releases AS
          SELECT
            recordings.id as recording_id,
            array_to_string(array_agg(distinct releases.name), ',') as release_name,
            array_agg(distinct releases.id) as release_ids,
            json_agg(distinct jsonb_build_object('id', releases.id, 'name', releases.name)) AS json_releases,
            min(releases.year) AS year
          FROM recordings
          INNER JOIN release_recordings rr ON rr.recording_id = recordings.id
          INNER JOIN releases ON releases.id = rr.release_id
          GROUP BY recordings.id
        ;
        CREATE UNIQUE INDEX joined_recording_releases_id ON joined_recording_releases (recording_id)
      SQL
    )
    puts "joined_recording_releases: #{result.cmd_tuples} rows affected"
    result = ApplicationRecord.connection.execute(
      <<~SQL
        DROP MATERIALIZED VIEW joined_recording_artists;
        CREATE MATERIALIZED VIEW joined_recording_artists AS
          SELECT
            recordings.id as recording_id
            ,array_agg(artists.id) as artist_ids
            ,array_to_string(array_agg(artists.name ORDER BY ra.cardinality), ',') as artist_name
            ,json_agg(json_build_object('id', artists.id, 'name', artists.name) ORDER BY ra.cardinality) AS json_artists
          FROM recordings
          INNER JOIN recording_artists ra ON ra.recording_id = recordings.id
          INNER JOIN artists            ON artists.id = ra.artist_id
          GROUP BY recordings.id
        ;
        CREATE UNIQUE INDEX joined_recording_artists_id ON joined_recording_artists (recording_id)
      SQL
    )
    puts "joined_recording_artists: #{result.cmd_tuples} rows affected"
    result = ApplicationRecord.connection.execute(
      <<~SQL
        DROP MATERIALIZED VIEW joined_release_artists;
        CREATE MATERIALIZED VIEW joined_release_artists AS
          SELECT
            releases.id as release_id
            ,array_agg(artists.id) as artist_ids
            ,array_to_string(array_agg(artists.name ORDER BY ra.cardinality), ',') as artist_name
            ,json_agg(json_build_object('id', artists.id, 'name', artists.name) ORDER BY ra.cardinality) AS json_artists
          FROM releases
          INNER JOIN release_artists ra ON ra.release_id = releases.id
          INNER JOIN artists            ON artists.id = ra.artist_id
          GROUP BY releases.id
        ;
        CREATE UNIQUE INDEX joined_release_artists_id ON joined_release_artists (release_id)
      SQL
    )
    puts "joined_release_artists: #{result.cmd_tuples} rows affected"
  end
end
