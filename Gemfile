source 'https://rubygems.org'

gem 'rails', '~> 5.2'
gem 'pg'
gem 'http'
gem 'puma', '~> 3.0'

gem 'devise', github: 'heartcombo/devise'
gem 'omniauth-spotify'
gem 'omniauth-rails_csrf_protection'

# --- Front-end

# gem 'webpacker'
gem 'sassc-rails'
gem 'uglifier', '>= 1.3.0'
gem 'will_paginate'

# --- Utility

gem 'immutable-struct'

# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'pry'

  gem 'rspec-rails'
  gem 'vcr'
  gem 'webmock'
  gem 'timecop'

  gem 'paydici-rubocop'
end

group :development do
  gem 'web-console'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end
