Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  devise_scope :user do
    get 'sign_in', to: redirect('/'), as: :new_user_session
    get 'sign_out', to: redirect('/'), as: :destroy_user_session
  end

  root to: 'plays#index', date: 'today'

  get 'lf' => 'application#lastfm'
  post 'lf' => 'application#lastfm'
  get 'mb' => 'application#musicbrainz'

  resources :playlists, only: [:index, :show]
  resources :playlist_recordings, only: [:create]

  resources :plays, only: [:index, :create]

  resources :artists, only: [:show, :index, :update] do
    collection do
      get :roster
    end
  end

  resources :releases, only: [:show, :index, :update] do
    collection do
      get :search
      get :import
      post :import
    end
  end

  resources :recordings, only: [:index, :show, :update]
end
