require 'rails_helper'

describe Connectors::MusicBrainz do

  it 'queries recordings' do
    response = VCR.use_cassette('musicbrainz_query_recording') do
      subject.query_recording('Triangle Walks', artist: 'Fever Ray')
    end

    doc = Nokogiri::XML(response)

    expect(doc.css('recording-list > recording > title').first.text).to eq 'Triangle Walks'
    expect(doc.css('recording-list > recording').first.css('release > title').first.text).to eq 'Fever Ray'
  end

end
