require 'rails_helper'

describe Connectors::LastFm do

  it 'gets recent plays' do
    Timecop.travel('2016-12-07') do
      response = VCR.use_cassette('lastfm_recent_plays') { subject.recent_tracks(from: Time.zone.now) }
      track_data = response.results.last

      expect(track_data[:name]).to eq 'The Donald'
      expect(track_data[:artist_name]).to eq 'A Tribe Called Quest'
      expect(track_data[:release_name]).to eq 'We got it from Here... Thank You 4 Your service'
    end
  end

  it 'gets earliest plays' do
    Timecop.travel('2016-12-07') do
      response = VCR.use_cassette('lastfm_earliest_plays') { subject.earliest_tracks }
      track_data = response.results.last

      expect(track_data[:name]).to eq 'Golden Hill'
      expect(track_data[:artist_name]).to eq 'Tristeza'
      expect(track_data[:release_name]).to eq 'Spine and Sensory'
    end
  end
end
