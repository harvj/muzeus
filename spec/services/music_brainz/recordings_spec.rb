require 'rails_helper'

describe MusicBrainz::Recordings do

  it 'works' do
    response = VCR.use_cassette('musicbrainz_query_recording') do
      MusicBrainz::Recordings.(name: 'Triangle Walks', artist: 'Fever Ray')
    end

    recording = response.results.first

    expect(recording[:name]).to eq 'Triangle Walks'
    expect(recording[:mbid]).to eq '54dd7a03-8413-48b9-b00a-1d436a924369'
    expect(recording[:length]).to eq 263280
    expect(recording[:artist_list].first[:name]).to eq 'Fever Ray'
    expect(recording[:release_list].first[:name]).to eq 'Fever Ray'
  end

end
