# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_12_30_014939) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "aliases", id: :serial, force: :cascade do |t|
    t.string "term", limit: 255
    t.string "variations", limit: 255
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "artists", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "mbid", limit: 255
    t.integer "category"
    t.integer "status", default: 0
    t.index ["category"], name: "index_artists_on_category"
  end

  create_table "match_recordings", id: :serial, force: :cascade do |t|
    t.integer "recording_id"
    t.integer "muzeus_match_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "muzeus_matches", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255
    t.string "artist_string", limit: 255
    t.string "release_string", limit: 255
    t.string "recording_string", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "playlist_recordings", force: :cascade do |t|
    t.bigint "playlist_id"
    t.bigint "recording_id"
    t.integer "position"
    t.index ["playlist_id"], name: "index_playlist_recordings_on_playlist_id"
    t.index ["recording_id"], name: "index_playlist_recordings_on_recording_id"
  end

  create_table "playlists", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "date"
  end

  create_table "plays", id: :serial, force: :cascade do |t|
    t.datetime "played_at"
    t.integer "recording_id"
    t.integer "user_id"
    t.integer "release_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["played_at"], name: "index_plays_on_played_at"
    t.index ["recording_id"], name: "index_plays_on_recording_id"
    t.index ["release_id"], name: "index_plays_on_release_id"
  end

  create_table "recording_artists", id: :serial, force: :cascade do |t|
    t.integer "recording_id"
    t.integer "artist_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cardinality", default: 0
    t.index ["artist_id"], name: "index_recording_artists_on_artist_id"
    t.index ["cardinality"], name: "index_recording_artists_on_cardinality"
    t.index ["recording_id"], name: "index_recording_artists_on_recording_id"
  end

  create_table "recordings", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255
    t.string "mbid", limit: 255
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "length"
    t.integer "category"
    t.index ["category"], name: "index_recordings_on_category"
    t.index ["length"], name: "index_recordings_on_length"
  end

  create_table "release_artists", id: :serial, force: :cascade do |t|
    t.integer "release_id"
    t.integer "artist_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cardinality", default: 0
    t.index ["artist_id"], name: "index_release_artists_on_artist_id"
    t.index ["cardinality"], name: "index_release_artists_on_cardinality"
    t.index ["release_id"], name: "index_release_artists_on_release_id"
  end

  create_table "release_recordings", id: :serial, force: :cascade do |t|
    t.integer "recording_id"
    t.integer "release_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "position"
    t.index ["recording_id"], name: "index_release_recordings_on_recording_id"
    t.index ["release_id"], name: "index_release_recordings_on_release_id"
  end

  create_table "releases", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "mbid", limit: 255
    t.integer "year"
    t.integer "month"
    t.integer "day"
    t.integer "category"
    t.integer "status", default: 0
    t.index ["category"], name: "index_releases_on_category"
  end

  create_table "user_artists", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "artist_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "category", default: 0
    t.integer "status", default: 0
    t.index ["artist_id"], name: "index_user_artists_on_artist_id"
    t.index ["category"], name: "index_user_artists_on_category"
    t.index ["status"], name: "index_user_artists_on_status"
    t.index ["user_id"], name: "index_user_artists_on_user_id"
  end

  create_table "user_recordings", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "recording_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "category", default: 0
    t.integer "status", default: 0
    t.integer "love", default: 0
    t.index ["category"], name: "index_user_recordings_on_category"
    t.index ["love"], name: "index_user_recordings_on_love"
    t.index ["recording_id"], name: "index_user_recordings_on_recording_id"
    t.index ["status"], name: "index_user_recordings_on_status"
    t.index ["user_id"], name: "index_user_recordings_on_user_id"
  end

  create_table "user_releases", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "release_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "category", default: 0
    t.integer "status", default: 0
    t.index ["category"], name: "index_user_releases_on_category"
    t.index ["release_id"], name: "index_user_releases_on_release_id"
    t.index ["status"], name: "index_user_releases_on_status"
    t.index ["user_id"], name: "index_user_releases_on_user_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "uid", limit: 255
    t.string "provider", limit: 255
    t.string "name", limit: 255
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "last_fm_id", limit: 255
    t.string "email"
    t.string "spotify_profile_url"
    t.string "spotify_access_token"
    t.string "spotify_refresh_token"
    t.datetime "spotify_token_expires_at"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
