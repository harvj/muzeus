class AddCategoryToArtists < ActiveRecord::Migration[5.1]
  def change
    add_column :artists, :category, :integer
    remove_column :artists, :rdio_key
    remove_column :artists, :sort_name
  end
end
