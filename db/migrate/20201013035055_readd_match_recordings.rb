class ReaddMatchRecordings < ActiveRecord::Migration[5.2]
  def change
    create_table "match_recordings", id: :serial, force: :cascade do |t|
      t.integer "recording_id"
      t.integer "muzeus_match_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
  end
end
