class AddRecordingIndexOnPlays < ActiveRecord::Migration[5.1]
  def change
    add_index :plays, :recording_id
  end
end
