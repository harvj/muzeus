class ChangeReleaseCatEnumToCategory < ActiveRecord::Migration[5.1]
  def change
    remove_column :releases, :art, :string
    rename_index :releases, 'index_releases_on_cat_enum', 'index_releases_on_category'
    remove_column :releases, :category, :string
    rename_column :releases, :cat_enum, :category
  end
end
