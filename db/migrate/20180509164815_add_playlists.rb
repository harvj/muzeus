class AddPlaylists < ActiveRecord::Migration[5.1]
  def change
    create_table :playlists do |t|
      t.string :name
      t.timestamps
    end

    create_table :playlist_recordings do |t|
      t.references :playlist, index: true
      t.references :recording, index: true
      t.integer :position
    end
  end
end
