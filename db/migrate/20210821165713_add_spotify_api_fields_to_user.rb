class AddSpotifyApiFieldsToUser < ActiveRecord::Migration[5.2]
  def change
    rename_column :users, :uri, :spotify_profile_url
    add_column :users, :spotify_access_token, :string
    add_column :users, :spotify_refresh_token, :string
    add_column :users, :spotify_token_expires_at, :datetime
  end
end
