class DropUnusedTables < ActiveRecord::Migration[5.2]
  def change
    drop_table :musicians
    drop_table :search_suggestions
  end
end
