class AddStatusToArtists < ActiveRecord::Migration[5.1]
  def change
    add_column :artists, :status, :integer, default: 0
  end
end
