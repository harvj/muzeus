class AddPersonalColumnsToUserTables < ActiveRecord::Migration[5.2]
  def change
    add_index :match_recordings, :recording_id
    add_index :match_recordings, :muzeus_match_id

    drop_table :musicians

    add_index :muzeus_matches, :name

    add_index :playlist_recordings, :playlist_id
    add_index :playlist_recordings, :recording_id

    add_column :playlists, :user_id, :integer
    add_index :playlists, :user_id

    add_index :plays, :user_id

    add_column :recordings, :year, :integer, default: 0
    add_index :recordings, :year
    add_column :recordings, :month, :integer, default: 0
    add_index :recordings, :month
    add_column :recordings, :day, :integer, default: 0
    add_index :recordings, :day

    add_index :releases, :year
    add_index :releases, :month
    add_index :releases, :day

    drop_table :search_suggestions

    add_index :user_artists, :user_id
    add_index :user_artists, :artist_id
    add_column :user_artists, :category, :integer, default: 0
    add_index :user_artists, :category
    add_column :user_artists, :status, :integer, default: 0
    add_index :user_artists, :status

    add_index :user_recordings, :user_id
    add_index :user_recordings, :recording_id
    add_column :user_recordings, :category, :integer, default: 0
    add_index :user_recordings, :category
    add_column :user_recordings, :status, :integer, default: 0
    add_index :user_recordings, :status
    add_column :user_recordings, :love, :integer, default: 0
    add_index :user_recordings, :love

    add_index :user_releases, :user_id
    add_index :user_releases, :release_id
    add_column :user_releases, :category, :integer, default: 0
    add_index :user_releases, :category
    add_column :user_releases, :status, :integer, default: 0
    add_index :user_releases, :status
  end
end
