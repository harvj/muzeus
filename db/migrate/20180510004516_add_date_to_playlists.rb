class AddDateToPlaylists < ActiveRecord::Migration[5.1]
  def change
    add_column :playlists, :date, :date
  end
end
