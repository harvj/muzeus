class AddCardinalities < ActiveRecord::Migration[5.2]
  def change
    add_column :recording_artists, :cardinality, :integer, default: 0
    add_index :recording_artists, :cardinality

    add_column :release_artists, :cardinality, :integer, default: 0
    add_index :release_artists, :cardinality

    remove_column :releases, :artist_id
    remove_column :recordings, :artist_id
  end
end
