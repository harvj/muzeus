class AddCategoryToRecordings < ActiveRecord::Migration[5.2]
  def change
    add_column :recordings, :category, :integer
    add_index :recordings, :category
    add_index :artists, :category
  end
end
