class AddStatusToReleases < ActiveRecord::Migration[5.1]
  def change
    add_column :releases, :status, :integer, default: 0
  end
end
