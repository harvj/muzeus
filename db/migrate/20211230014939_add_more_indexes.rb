class AddMoreIndexes < ActiveRecord::Migration[5.2]
  def change
    add_index :recordings, :length

    add_index :release_artists, :artist_id
    add_index :release_artists, :release_id

    add_index :release_recordings, :release_id
    add_index :release_recordings, :recording_id

    add_index :recording_artists, :artist_id
    add_index :recording_artists, :recording_id

    add_index :plays, :played_at
  end
end
