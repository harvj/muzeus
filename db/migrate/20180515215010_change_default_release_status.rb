class ChangeDefaultReleaseStatus < ActiveRecord::Migration[5.1]
  def change
    change_column :releases, :status, :integer, default: 0
  end
end
