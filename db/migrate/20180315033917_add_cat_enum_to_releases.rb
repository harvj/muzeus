class AddCatEnumToReleases < ActiveRecord::Migration[5.1]
  def change
    add_column :releases, :cat_enum, :integer
    add_index :releases, :cat_enum
  end
end
