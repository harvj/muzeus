class RemoveRdioKeyFromReleases < ActiveRecord::Migration[5.1]
  def change
    remove_column :releases, :rdio_key, :string
    add_column :releases, :artist_id, :integer
    add_index :releases, :artist_id
  end
end
